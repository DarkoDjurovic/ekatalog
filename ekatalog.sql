-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 29, 2016 at 05:19 PM
-- Server version: 5.7.9
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ekatalog`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `sex` tinyint(1) NOT NULL COMMENT '0-zensko,1-musko',
  `JMBG` int(13) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `name`, `last_name`, `sex`, `JMBG`, `active`) VALUES
(1, 'admin', '06828b023332bf786263c93864ab9a173f8fdff44223d3a2c76d8487e6c93aaa3f99f251c08ed5a5ba1500fbda02af0719a4aac561302b4f0bd3eadfbae19082', 'Darko', 'Djurovic', 1, 123465789, 1),
(2, 'root', '4c52e9a3054f3e73b7a5e50dc208d785901bf045fdaf212ba025abf2eb44ecd8e4869b386a1edd38cdf989f7c4be3bb3b407e86d80a6f28490a901eeef69e0f8', 'Nikola', 'Nikolić', 1, 987654321, 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Mobilni telefoni'),
(2, 'Kompjuteri'),
(3, 'Touchscreen'),
(4, 'Windows'),
(5, 'Android'),
(6, 'iOS');

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
CREATE TABLE IF NOT EXISTS `devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `manufacturer` int(11) NOT NULL,
  `offer_type_id` int(11) DEFAULT NULL,
  `price` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `manufacturer` (`manufacturer`),
  KEY `offer_type_id` (`offer_type_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`id`, `name`, `description`, `manufacturer`, `offer_type_id`, `price`, `user_id`, `date_added`) VALUES
(48, 'iphone 6', 'OPŠTE	\r\n2G mreža	GSM 850 / 900 / 1800 / 1900 - A1549 (GSM), A1549 (CDMA), A1586\r\n3G mreža	HSDPA 850 / 900 / 1700 / 1900 / 2100 - A1549 (GSM), A1549 (CDMA), A1586\r\n4G mreža	LTE 700/800/850/900/1700/1800/1900/2100/2600 (1/2/3/4/5/7/8/13/17/18/19/20/25/26/28/29) - A1549 (GSM), A1549 (CDMA)\r\nSIM	Nano-SIM\r\nNajavljen	2014, Septembar\r\nStatus	Dostupan. U prodaji od 2014, Septembar', 1, 1, 500, 1, '2016-09-05 17:15:11'),
(49, 'samsung galaxy s6', '\r\nOPŠTE	\r\n2G mreža	GSM 850 / 900 / 1800 / 1900\r\n3G mreža	HSDPA 850 / 900 / 1900 / 2100\r\n4G mreža	LTE\r\nSIM	Nano-SIM\r\nNajavljen	2015, Mart\r\nStatus	Dostupan. U prodaji od 2015, April', 2, 3, 650, 1, '2016-09-05 17:16:54'),
(50, 'nokia lumia 930', 'Market		\r\nStatus : Aktuelan\r\nModel : Lumia 930\r\nMreže : 850Mhz, 900Mhz, 1800Mhz, 1900Mhz, HSDPA, LTE,\r\nKućište		\r\nDimenzije : 137 x 71 x 9.8 mm\r\nTežina : 167 grama\r\nOblik : touch-screen,\r\nTastatura : nema,\r\nProcesor		\r\nProcesor : Quad-core\r\nTakt : 2200 MHz\r\nBaterija		\r\nVrsta : Li-Ion\r\nKapacitet : 2420 mAh\r\nFabrička oznaka : BV-5QW\r\nOstalo		\r\nOstalo : vibracija, kompas, akcelerometar,\r\n', 2, 2, 300, 1, '2016-09-05 17:18:28');

--
-- Triggers `devices`
--
DROP TRIGGER IF EXISTS `device_edit`;
DELIMITER $$
CREATE TRIGGER `device_edit` BEFORE UPDATE ON `devices` FOR EACH ROW BEGIN
        IF NEW.manufacturer NOT RLIKE BINARY '^[0-9]+$' THEN
            SIGNAL SQLSTATE '45000';
        END IF;
        IF NEW.offer_type_id NOT RLIKE BINARY '^[0-9]+$' THEN
            SIGNAL SQLSTATE '45000';
        END IF;
        IF NEW.price NOT RLIKE BINARY '^[0-9]+$' THEN
            SIGNAL SQLSTATE '45000';
        END IF;
        IF NEW.user_id NOT RLIKE BINARY '^[0-9]+$' THEN
            SIGNAL SQLSTATE '45000';
        END IF;
    END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `device_insert`;
DELIMITER $$
CREATE TRIGGER `device_insert` BEFORE INSERT ON `devices` FOR EACH ROW BEGIN
        IF NEW.manufacturer NOT RLIKE BINARY '^[0-9]+$' THEN
            SIGNAL SQLSTATE '45000';
        END IF;
        IF NEW.offer_type_id NOT RLIKE BINARY '^[0-9]+$' THEN
            SIGNAL SQLSTATE '45000';
        END IF;
        IF NEW.price NOT RLIKE BINARY '^[0-9]+$' THEN
            SIGNAL SQLSTATE '45000';
        END IF;
        IF NEW.user_id NOT RLIKE BINARY '^[0-9]+$' THEN
            SIGNAL SQLSTATE '45000';
        END IF;
    END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `device_category`
--

DROP TABLE IF EXISTS `device_category`;
CREATE TABLE IF NOT EXISTS `device_category` (
  `device_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  KEY `device_id` (`device_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `device_category`
--

INSERT INTO `device_category` (`device_id`, `category_id`) VALUES
(48, 1),
(48, 3),
(49, 1),
(49, 3),
(50, 1),
(50, 3),
(48, 6),
(49, 5),
(50, 4);

-- --------------------------------------------------------

--
-- Table structure for table `device_photo`
--

DROP TABLE IF EXISTS `device_photo`;
CREATE TABLE IF NOT EXISTS `device_photo` (
  `device_id` int(11) NOT NULL,
  `photo_id` int(11) NOT NULL,
  KEY `device_id` (`device_id`),
  KEY `photo_id` (`photo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `device_photo`
--

INSERT INTO `device_photo` (`device_id`, `photo_id`) VALUES
(48, 81),
(48, 82),
(48, 83),
(49, 84),
(49, 85),
(49, 86),
(50, 87),
(50, 88),
(50, 89);

-- --------------------------------------------------------

--
-- Table structure for table `manufacturers`
--

DROP TABLE IF EXISTS `manufacturers`;
CREATE TABLE IF NOT EXISTS `manufacturers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manufacturers`
--

INSERT INTO `manufacturers` (`id`, `name`) VALUES
(1, 'Apple'),
(3, 'Microsoft'),
(2, 'Samsung');

-- --------------------------------------------------------

--
-- Table structure for table `offer_type`
--

DROP TABLE IF EXISTS `offer_type`;
CREATE TABLE IF NOT EXISTS `offer_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offer_type`
--

INSERT INTO `offer_type` (`id`, `name`) VALUES
(1, 'Akcija'),
(2, 'Rasprodaja'),
(3, 'Novo');

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` text NOT NULL,
  `is_main` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `file_name`, `is_main`) VALUES
(1, 'webmonks1.png', 1),
(2, '1.jpg', 0),
(3, '2.png', 0),
(4, '_1467071799.jpg', 1),
(5, '_1467071956.jpg', 1),
(6, '_1467071971.jpg', 1),
(7, '_1467072022.jpg', 1),
(8, '_1467072193.jpg', 1),
(9, '_1467072193.jpg', 0),
(10, '_1467072193.jpg', 0),
(11, '_1467072364.jpg', 1),
(12, '_1467072364.jpg', 0),
(13, '_1467072364.jpg', 0),
(14, '_1467072416.jpg', 1),
(15, '_1467072416.jpg', 0),
(16, '_1467072416.jpg', 0),
(17, '_0.45859400 1467072597.jpg', 1),
(18, '_0.04051200 1467072598.jpg', 0),
(19, '_0.10874800 1467072599.jpg', 0),
(20, '_0.594318001467072664.jpg', 1),
(21, '_0.669812001467072664.jpg', 0),
(22, '_0.709306001467072664.jpg', 0),
(23, '_0.248140001471684975.jpg', 1),
(24, '_0.590470001471684975.jpg', 0),
(25, '_0.952849001471684975.jpg', 0),
(26, '_0.863307001471685083.jpg', 1),
(27, '_0.288890001471685084.jpg', 0),
(28, '_0.788979001471685084.jpg', 0),
(29, '_0.657015001471685426.jpg', 1),
(30, '_0.165810001471685427.jpg', 0),
(31, '_0.629246001471685427.jpg', 0),
(32, '_0.732723001471686320.jpg', 1),
(33, '_0.224250001471686321.jpg', 0),
(34, '_0.737214001471686321.jpg', 0),
(35, '_0.358456001471686402.jpg', 1),
(36, '_0.979066001471686402.jpg', 0),
(37, '_0.717934001471686403.jpg', 0),
(38, '_0.211771001471686478.jpg', 1),
(39, '_0.935910001471686478.jpg', 0),
(40, '_0.655321001471686479.jpg', 0),
(41, '_0.876746001471689803.jpg', 1),
(42, '_0.555390001471689804.jpg', 0),
(43, '_0.331590001471689805.jpg', 0),
(44, '_0.099304001471689864.jpg', 1),
(45, '_0.479380001471689864.jpg', 0),
(46, '_0.770577001471689864.jpg', 0),
(47, '_0.465160001471689999.jpg', 1),
(48, '_0.748353001471689999.jpg', 0),
(49, '_0.045323001471690000.jpg', 0),
(50, '_0.446383001471690192.jpg', 1),
(51, '_0.731764001471690192.jpg', 0),
(52, '_0.028852001471690193.jpg', 0),
(53, '_0.318851001471690318.jpg', 1),
(54, '_0.603656001471690318.jpg', 0),
(55, '_0.907689001471690318.jpg', 0),
(56, '_0.461137001471690502.jpg', 1),
(57, '_0.773996001471690502.jpg', 0),
(58, '_0.047048001471690503.jpg', 0),
(59, '_0.566336001471690561.jpg', 1),
(60, '_0.832742001471690561.jpg', 0),
(61, '_0.110477001471690562.jpg', 0),
(62, '_0.981147001471690719.jpg', 1),
(63, '_0.287496001471690720.jpg', 0),
(64, '_0.622908001471690720.jpg', 0),
(65, '_0.360443001471702717.jpg', 1),
(66, '_0.664985001471702717.jpg', 0),
(67, '_0.016717001471702718.jpg', 0),
(68, '_0.874213001471703338.jpg', 1),
(69, '_0.210679001471703339.jpg', 0),
(70, '_0.526109001471703339.jpg', 0),
(71, '_0.029972001473090473.jpg', 1),
(72, '_0.804779001473090473.jpg', 0),
(73, '_0.222569001473090857.jpg', 1),
(74, '_0.760874001473090857.jpg', 0),
(75, '_0.309467001473090920.jpg', 1),
(76, '_0.800092001473090920.jpg', 0),
(77, '_0.484265001473090977.jpg', 1),
(78, '_0.192295001473090978.jpg', 0),
(79, '_0.962441001473094921.jpg', 1),
(80, '_0.519602001473094922.jpg', 0),
(81, '_0.191246001473095712.jpg', 1),
(82, '_0.734909001473095712.jpg', 0),
(83, '_0.859352001473095713.jpg', 0),
(84, '_0.989497001473095814.jpg', 1),
(85, '_0.536727001473095815.jpg', 0),
(86, '_0.179820001473095816.jpg', 0),
(87, '_0.669715001473095908.jpg', 1),
(88, '_0.261497001473095909.jpg', 0),
(89, '_0.920963001473095909.jpg', 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `devices`
--
ALTER TABLE `devices`
  ADD CONSTRAINT `manufacturer_constraint` FOREIGN KEY (`manufacturer`) REFERENCES `manufacturers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `offer_type_id_constraint` FOREIGN KEY (`offer_type_id`) REFERENCES `offer_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_id_constraint` FOREIGN KEY (`user_id`) REFERENCES `admins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `device_category`
--
ALTER TABLE `device_category`
  ADD CONSTRAINT `category_id_constraint` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `device_id_constraint` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `device_photo`
--
ALTER TABLE `device_photo`
  ADD CONSTRAINT `device_id_constraint2` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `photo_id_constraint` FOREIGN KEY (`photo_id`) REFERENCES `photos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
