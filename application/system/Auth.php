	<?php

	/**
	 * implementacija autentifikacije
	 */
	final class Auth{
		/**
		 * dohvatanje korisnickog id-a
		 * @return null
		 */
		public static function getUserId(){
			$user = Session::get("user");
			if(isset($user) && isset($user->id))
				return $user->id;
			else return null;
		}

		/**
		 * dohvatnaje korisnickog imena
		 * @return null
		 */
		public static function getUserName(){
			$user = Session::get("user");
			if(isset($user) && isset($user->name))
				return $user->name;
			else return null;
		}

		/**
		 * provera ulogovanosti korisnika
		 * @return bool
		 */
		public static function isLoggedIn(){
			return Session::get("logedin") === true;
		}

		/**
		 * prijava na sistem
		 * @param $user
		 */
		public static function logIn($user){
			Session::set("logedin",true);
			Session::set("user",$user);
		}

		/**
		 * odjava sa sistema
		 */
		public static function logOut(){
			Session::del("logedin");
			Session::del("user");
		}
	}
