<?php

/**
 * implementacija ruta
 */
final class Routes{
	/**
	 * vraca niz svih sistemskih ruta zajedno sa kontrolerima i f-jama koje treba da se izvrse
	 * @return array
	 */
	public static final function get(){
		return [
			[
	            'pattern'    => '|^/about-us/?$|',
	            'controller' => 'AboutUs',
	            'action'     => 'index'
	        ],
			[
	            'pattern'    => '|^/contact/?$|',
	            'controller' => 'Contact',
	            'action'     => 'index'
	        ],
			[
	            'pattern'    => '|^/device/new/?$|',
	            'controller' => 'Admin',
	            'action'     => 'insert'
            ],
			[
	            'pattern'    => '|^/device/edit/([a-z0-9\-]+)/?.*$|',
	            'controller' => 'Admin',
	            'action'     => 'edit'
            ],
			[
	            'pattern'    => '|^/device/delete/([a-z0-9\-]+)/?.*$|',
	            'controller' => 'Admin',
	            'action'     => 'delete'
            ],
			[
	            'pattern'    => '|^/admin/?.*$|',
	            'controller' => 'Admin',
	            'action'     => 'index'
            ],
			[
	            'pattern'    => '|^/logout/?$|',
	            'controller' => 'Index',
	            'action'     => 'logout'
            ],
			[
	            'pattern'    => '|^/login/?$|',
	            'controller' => 'Index',
	            'action'     => 'login'
            ],
			[
	            'pattern'    => '|^/device/([a-z0-9\-]+)/?$|',
	            'controller' => 'Device',
	            'action'     => 'show'
            ],
			[
	            'pattern'    => '|^/category/([a-z0-9\-]+)/?$|',
	            'controller' => 'Index',
	            'action'     => 'category'
            ],
			[
	            'pattern'    => '|^/category/([a-z0-9\-]+)/([a-z0-9\-]+)/?$|',
	            'controller' => 'Index',
	            'action'     => 'category'
            ],
			[
	            'pattern'    => '|^/([a-z0-9\-]+)/?$|',
	            'controller' => 'Index',
	            'action'     => 'index'
            ],
			[
	            'pattern'    => '|^/search/?\?.*?$|',
	            'controller' => 'Index',
	            'action'     => 'index'
	        ],
			[
	            'pattern'    => '|^/?$|',
	            'controller' => 'Index',
	            'action'     => 'index'
	        ]
		];
	}
}