<?php

/**
 * implementacija sesije
 */
final class Session{
	/**
	 * zapocinjanje sesije
	 */
	public static final function start(){
		session_start();
	}

	/**
	 * zatvaranje sesije
	 */
	public static final function end(){
		self::clear();
        session_destroy();
	}

	/**
	 * ciscenje sesije
	 */
	public static final function clear(){
		foreach($_SESSION as $key => $val){
			self::del($_SESSION[$key]);
		}
	}

	/**
	 * provera da li podatak postoji u sesiji
	 * @param $key
	 * @return bool
	 */
	public static final function exists($key){
		return isset($_SESSION[$key]);
	}

	/**
	 * dodavanje podatka u sesiju
	 * @param $key
	 * @param $val
	 */
	public static final function set($key, $val){
		$_SESSION[$key] = $val;
	}

	/**
	 * brisanje podatka iz sesije
	 * @param $key
	 */
	public static final function del($key){
		unset($_SESSION[$key]);
	}

	/**
	 * dohvatanje podatka iz sesije
	 * @param $key
	 * @return bool
	 */
	public static final function get($key){
		if(self::exists($key))
			return $_SESSION[$key];
		else return false;
	}

	/**
	 * dodavanje poruke u sesiju
	 * @param $message
	 */
	public static final function addMessage($message){
		$messages = self::get("messages");
		$messages[] = $message;
		self::del("messages");
		self::set("messages",$messages);
	}

	/**
	 * dohvatanje i ciscenje svih podataka iz sesije
	 * @return bool
	 */
	public static final function getMessages(){
		$messages = self::get("messages");
		self::del("messages");
		return $messages;
	}
}