<?php

/**
 * helper klasa
 */
final class Helper{
	/**
	 * provera da li se jedan string zavrsava drugim
	 * @param $string
	 * @param $test
	 * @return bool
	 */
	public static final function endswith($string, $test) {
	    $strlen = strlen($string);
	    $testlen = strlen($test);
	    if ($testlen > $strlen) return false;
	    return substr_compare($string, $test, $strlen - $testlen, $testlen) === 0;
	}

	/**
	 * dohvatanje slike iz foldera
	 * @param $photoObj
	 * @return string
	 */
	public static final function getImage($photoObj){
		if($photoObj !== null && isset($photoObj->file_name))
			return "/data/photos/".$photoObj->file_name;
		else
			return "/resources/images/default_image.jpg";
	}

	/**
	 * dohvatanje smanjene verzije slike
	 * @param $photoObj
	 * @return string
	 */
	public static final function getImageThumb($photoObj){
		if($photoObj !== null && isset($photoObj->file_name))
			return "/data/photos/".$photoObj->file_name.".thumb.jpg";
		else
			return "/resources/images/default_image.jpg.thumb.jpg";
	}

	/**
	 * kreiranje include putanje za include-ovanje odredjenog zajednockog view-a
	 * @param $name
	 * @return string
	 */
	public static final function includePartial($name){
		return "application/views/Partials/$name.php";
	}

	/**
	 * redirectovanje korisnika na odredjen url
	 * @param $link
	 * @param string $base
	 */
	public static function redirect($link, $base = Config::BASE) {
        ob_clean();
        header('Location: ' . $base . $link);
        exit;
    }
}