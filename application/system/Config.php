<?php

/**
 * implementacija konfiguracije
 */
final class Config {
	const DB_HOST = 'localhost';
    const DB_USER = 'root';
    const DB_PASS = '';
    const DB_BASE = 'ekatalog';

    const BASE = 'http://dev.webmonks.ekatalog/';
    const PATH = '/';

    const SALT = '09560ughkjfgh08u35608utrgho8u54yjhi0569uyrth';

    const PHOTO_LIBRARY_PATH = 'data/photos/';
	const DEFAULT_PER_PAGE = 20;
}