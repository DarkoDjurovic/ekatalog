<?php

/**
 * kontroler za admin sekciju
 */
class AdminController extends Controller{

	/**
	 * nasledjena f-ja open, proverava ulogovanost korisnika
	 */
	public function open(){
		parent::open();
		if(!Auth::isLoggedIn()){
			$this->addMessage("Niste ulogovani!");
			Helper::redirect("");
		}
	}

	/**
	 * ucitavanje stranice
	 * @param int $page
	 */
	public function index($page = 1){
		$this->setViewData('pageTitle', 'E-katalog');
		$this->setViewData('categories', CategoryModel::getAll());
		$this->setViewData('offer_types', OfferTypeModel::getAll());
		$this->setViewData('manufacturers', ManufacturerModel::getAll());

		$search = filter_input(INPUT_GET, 'search');
		if(isset($search) && !empty($search)){
			$this->setViewData('search', $search);
			$where = "name like '%$search%' or description like '%$search%'";
			if(is_numeric($search))
				$where .= " or price = $search";
			$devices = DeviceModel::getPagedWithCondition($where, -1, $page);
		} else {
			$devices = DeviceModel::getPaged(-1, $page);
		}
		$this->setViewData('devices', $devices);
	}

	/**
	 * brisanje artikla
	 * @param $id
	 */
	public function delete($id){
		$id = intval($id);

		DeviceModel::delete($id);

		Helper::redirect(Session::get("last_request"));
	}

	/**
	 * dodavanje artikla
	 */
	public function insert(){
		$insert = filter_input(INPUT_POST, 'insert');
		if(isset($insert)){
			$name = filter_input(INPUT_POST, 'name');
			$description = filter_input(INPUT_POST, 'description');
			$price = filter_input(INPUT_POST, 'price');
			$manufacturer = filter_input(INPUT_POST, 'manufacturer');
			$categories = $_POST['categories'];
			$offer_type = filter_input(INPUT_POST, 'offer_type');
			if(empty($offer_type))
				$offer_type = null;

			$insert_id = DeviceModel::add($name, $description, $manufacturer, $offer_type, $price, $categories);

			$upload_name = PhotoModel::upload($_FILES['image']);
			PhotoModel::add($upload_name, $insert_id, 1);
			$_FILES['images'] = PhotoModel::reArrayFiles($_FILES['images']);
			foreach($_FILES['images'] as $image){
				$upload_name = PhotoModel::upload($image);
				PhotoModel::add($upload_name, $insert_id, 0);
			}

			if($insert_id)
				Helper::redirect("device/".$insert_id);
			else{
				$this->addMessage("Dodavanje nije uspelo!");
				Helper::redirect(Session::get("last_request"));
			}
		} else {
			$device = new stdClass();
			$device->name = '';
			$device->description = '';
			$device->price = '';
			$device->manufacturer = new stdClass();
			$device->manufacturer->id = -1;
			$device->categories = array();
			$device->offer = new stdClass();
			$device->offer->id = -1;

			$this->setViewData('mode', 'new');
			$this->setViewData('pageTitle', 'Unos proizvoda');
			$this->setViewData('categories', CategoryModel::getAll());
			$this->setViewData('offer_types', OfferTypeModel::getAll());
			$this->setViewData('manufacturers', ManufacturerModel::getAll());

			$this->setViewData('device',$device);
		}
	}

	/**
	 * izmena artikla
	 * @param $id
	 */
	public function edit($id){
		$edit = filter_input(INPUT_POST, 'edit');
		if(isset($edit)) {
			$name = filter_input(INPUT_POST, 'name');
			$description = filter_input(INPUT_POST, 'description');
			$price = filter_input(INPUT_POST, 'price');
			$manufacturer = filter_input(INPUT_POST, 'manufacturer');
			$categories = $_POST['categories'];
			$offer_type = filter_input(INPUT_POST, 'offer_type');
			if (empty($offer_type))
				$offer_type = null;

			DeviceModel::edit($id, $name, $description, $manufacturer, $offer_type, $price, $categories);
			Helper::redirect("device/" . $id);
		} else {
			$device = DeviceModel::getById($id);

			$this->setViewData('mode', 'edit/' . $id);
			$this->setViewData('pageTitle', 'Unos proizvoda');
			$this->setViewData('categories', CategoryModel::getAll());
			$this->setViewData('offer_types', OfferTypeModel::getAll());
			$this->setViewData('manufacturers', ManufacturerModel::getAll());

			$this->setViewData('device',$device);
		}
	}

	/**
	 * nasledjena f-ja close
	 */
	public function close(){
		parent::close();
	}


}