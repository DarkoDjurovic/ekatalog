<?php

/**
 * kontroler naslovne stranice
 */
class IndexController extends Controller{
	/**
	 * nasledjena f-ja open
	 */
	public function open(){
		parent::open();
	}

	/**
	 * nasledjena f-ja close
	 */
	public function close(){
		parent::close();
	}

	/**
	 * ucitavanje index stranice
	 * @param int $page
	 */
	public function index($page = 1){
		$where = "";
		$delimiter = "";
		if(isset($_GET['price_from']) && intval($_GET['price_from']) > 0) {
			$where_var = filter_input(INPUT_GET, 'price_from', FILTER_SANITIZE_NUMBER_INT);
			$this->setViewData('price_from', $where_var);
			$where .= $delimiter . "price >= " . $where_var;
			$delimiter = " and ";
		}
		if(isset($_GET['price_to']) && intval($_GET['price_to']) > 0) {
			$where_var = filter_input(INPUT_GET, 'price_to', FILTER_SANITIZE_NUMBER_INT);
			$this->setViewData('price_to', $where_var);
			$where .= $delimiter . "price <= " . $where_var;
			$delimiter = " and ";
		}
		if(isset($_GET['offer_type']) && is_array($_GET['offer_type']) && count($_GET['offer_type']) > 0) {
			$where_var = $_GET['offer_type'];
			$this->setViewData('offer_types_selected', $where_var);
			$second_where = "(";
			$second_delimiter = "";
			foreach($where_var as $wv){
				$second_where .= $second_delimiter . "offer_type_id = " . $wv;
				$second_delimiter = " or ";
			}
			$second_where .= ")";
			$where .= $delimiter . $second_where;
			$delimiter = " and ";
		}
		if(isset($_GET['manufacturer']) && is_array($_GET['manufacturer']) && count($_GET['manufacturer']) > 0) {
			$where_var = $_GET['manufacturer'];
			$this->setViewData('manufacturers_selected', $where_var);
			$second_where = "(";
			$second_delimiter = "";
			foreach($where_var as $wv){
				$second_where .= $second_delimiter . "manufacturer = " . $wv;
				$second_delimiter = " or ";
			}
			$second_where .= ")";
			$where .= $delimiter . $second_where;
		}

		echo $where;

		$this->setViewData('pageTitle', 'E-katalog');
		$this->setViewData('categories', CategoryModel::getAll());
		$this->setViewData('offer_types', OfferTypeModel::getAll());
		$this->setViewData('manufacturers', ManufacturerModel::getAll());

		$devices = DeviceModel::getPagedWithCondition($where, -1, $page);
		$this->setViewData('devices', $devices);
	}

	/**
	 * ucitavanje listinga odredjene kategorije
	 * @param $id
	 * @param int $page
	 */
	public function category($id, $page = 1){
		$this->setViewData('pageTitle', 'E-katalog');
		$this->setViewData('categories', CategoryModel::getAll());
		$this->setViewData('selectedCategory', CategoryModel::getById($id));
		$this->setViewData('offer_types', OfferTypeModel::getAll());
		$this->setViewData('manufacturers', ManufacturerModel::getAll());

		$devices = DeviceModel::getPaged($id, $page);
		$this->setViewData('devices', $devices);
	}

	/**
	 * prijava korisnika na sistem
	 */
	public function login(){
		$username = filter_input(INPUT_POST, 'username');
		$password = filter_input(INPUT_POST, 'password');

		$admin = AdminModel::getByUsernameAndPassword($username, $password);
		if($admin && isset($admin->id) && AdminModel::isActive($admin->id)){
			Auth::logIn($admin);
			$this->addMessage("Uspesan login!");
			Helper::redirect("admin/");
		}
		else{
			$this->addMessage("Pogrešni podaci za prijavu!");
			Helper::redirect("");
		}
	}

	/**
	 * odjava korisnika sa sistema
	 */
	public function logout(){
		Auth::logOut();
		Helper::redirect("");
	}
}