<?php

/**
 * osnovna klasa kontrolera
 */
class Controller{

	/**
	 * niz u kome kontroler cuva rezultat svojih f-ja, kasnije se ukljucuje u view
	 * @var array
	 */
	private $data = [];

	/**
	 * f-ja koja se izvrsava pre svih ostalih
	 */
	public function open(){}

	/**
	 * f-ja nakon svih ostalih, snima poslednji korisnicki zahtev ukoliko treba korisnika da vratimo korak unazad
	 */
	public function close(){
		Session::set("last_request", substr(filter_input(INPUT_SERVER, 'REQUEST_URI'), 1));
	}

	/**
	 * dodavanje poruke u sesiju
	 * @param $message
	 */
	protected function addMessage($message){
		Session::addMessage($message);
	}

	/**
	 * dohvatanje svih sesijskih poruka
	 * @return bool
	 */
	public function getMessages(){
		return Session::getMessages();
	}

	/**
	 * snimanje podatka u data niz
	 * @param $key
	 * @param $val
	 */
	protected function setViewData($key, $val){
		$this->data[$key] = $val;
	}

	/**
	 * dohvatanje svih podataka potrebnih za ucitvanje view-a
	 * @return array
	 */
	public function getData(){
		$this->data['messages'] = $this->getMessages();
		return $this->data;
	}
}