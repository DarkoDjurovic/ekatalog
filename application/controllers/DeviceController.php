<?php

/**
 * kontroler stranice uredjaja
 */
class DeviceController extends Controller{
	/**
	 * nasledjena f-ja open
	 */
	public function open(){
		parent::open();
	}

	/**
	 * nasledjena f-ja close
	 */
	public function close(){
		parent::close();
	}

	/**
	 * ucitavanje pregleda odredjenog uredjaja
	 * @param $id
	 */
	public function show($id){
		$device = DeviceModel::getById($id);
		$this->setViewData('pageTitle', 'Uredjaj: '.$device->name);
		$this->setViewData('categories', CategoryModel::getAll());

		$this->setViewData('device', $device);
	}
}