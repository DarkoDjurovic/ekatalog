<?php

/**
 * kontroler stranice o nama
 */
class ContactController extends Controller{
	/**
	 * nasledjena f-ja open
	 */
	public function open(){
		parent::open();
	}

	/**
	 * nasledjena f-ja close
	 */
	public function close(){
		parent::close();
	}

	/**
	 * ucitavanje index stranice
	 */
	public function index(){
		$this->setViewData('pageTitle', 'Kontakt');
		$this->setViewData('categories', CategoryModel::getAll());
	}
}