<div class="row">
    <div class="col-md-6">
        <img class="img-responsive" src="<?php echo $device->photo; ?>"  alt="<?php echo $device->name; ?>"/>
    </div>
    <div class="col-md-6">
        <table class="table table-striped">
            <tr>
                <td>
                    <p>Ime</p>
                </td>
                <td>
                    <p><?php echo $device->name; ?></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Opis</p>
                </td>
                <td>
                    <p><?php echo $device->description; ?></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Cena</p>
                </td>
                <td>
                    <p><?php echo $device->price; ?>e</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Vreme dodavanja:</p>
                </td>
                <td>
                    <p><?php echo $device->date_added; ?></p>
                </td>
            </tr>
            <?php
            if($device->offer) {
            ?>
            <tr>
                <td>
                    <p>Ponuda:</p>
                </td>
                <td>
                    <p><?php echo $device->offer->name; ?></p>
                </td>
            </tr>
            <?php
            }
            ?>
            <tr>
                <td>
                    <p>Kategorije:</p>
                </td>
                <td>
                    <p><?php
                        $delimiter = '';
                        foreach($device->categories as $category){
                            echo $delimiter.$category->name;
                            $delimiter = ',';
                        }
                        ?>
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Proizvođač:</p>
                </td>
                <td>
                    <p><?php echo $device->manufacturer->name; ?></p>
                </td>
            </tr>
        </table>
    </div>
    <div class="col-md-12">
        <div id="links">
            <?php
            foreach($device->photos as $dP) {
            ?>
            <a href="<?php echo $dP; ?>" title="<?php echo $device->name; ?>" data-gallery>
                <img src="<?php echo $dP; ?>" alt="<?php echo $device->name; ?>">
            </a>
            <?php
            }
            ?>
        </div>
    </div>
</div>