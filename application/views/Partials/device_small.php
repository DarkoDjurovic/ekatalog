<a href="<?php echo Config::BASE.'device/'.$device->id; ?>">
    <div class="row">
        <div class="col-md-12 text-center">
            <h3><?php echo $device->name; ?></h3>
        </div>
        <div class="col-md-3">
            <img class="img-responsive" src="<?php echo $device->photo_thumb; ?>"  alt="<?php echo $device->name; ?>"/>
        </div>
        <div class="col-md-9">
            <p><?php echo $device->description; ?></p>

            <p>Cena: <?php echo $device->price; ?>e</p>
        </div>
    </div>
</a>