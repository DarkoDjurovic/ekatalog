<div class="row">
    <div class="col-md-3">
        <img class="img-responsive" src="<?php echo $device->photo; ?>" alt="<?php echo $device->name; ?>"/>
    </div>
    <div class="col-md-3">
        <h3><?php echo $device->name; ?></h3>
    </div>
    <div class="col-md-3">
        <p><?php echo $device->description; ?></p>

        <p>Cena: <?php echo $device->price; ?>e</p>
    </div>
    <div class="col-md-1">
        <a class="btn btn-danger" href="<?php echo Config::BASE.'device/delete/'.$device->id; ?>">
            Obriši
        </a>
    </div>
    <div class="col-md-1">
        <a class="btn btn-info" href="<?php echo Config::BASE.'device/edit/'.$device->id; ?>">
            Izmeni
        </a>
    </div>
    <div class="col-md-1">
        <a class="btn btn-success" href="<?php echo Config::BASE.'device/'.$device->id; ?>">
            Pogledaj
        </a>
    </div>
</div>
