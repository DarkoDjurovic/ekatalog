<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">E-katalog</a>
            <a class="navbar-brand" href="/about-us">O nama</a>
	        <a class="navbar-brand" href="/contact">Kontakt</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <?php
                if(!Auth::isLoggedIn()) {
                ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">Kategorije <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <?php
                        $categoryClass = "";
                        if (!isset($viewData['selectedCategory']))
                            $categoryClass = "active";
                        ?>
                        <li class="<?php echo $categoryClass; ?>"><a href="/">Sve</a></li>
                        <li role="separator" class="divider"></li>
                        <?php
                        foreach ($viewData['categories'] as $category) {
                            if (isset($viewData['selectedCategory']) && $viewData['selectedCategory']->id === $category->id)
                                $categoryClass = "active";
                            else
                                $categoryClass = "";
                            ?>
                            <li class="<?php echo $categoryClass; ?>"><a
                                    href="<?php echo '/category/' . $category->id; ?>"><?php echo $category->name; ?></a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </li>
                <?php
                }
                ?>
            </ul>
            <?php
            if(Auth::isLoggedIn()) {
            ?>
            <ul class="nav navbar-nav navbar-right">
                <li><a>Ulogovani ste kao: <i><?php echo Auth::getUserName(); ?></i></a></li>
                <li><a href="/admin">Admin panel</a></li>
                <li><a href="/logout">Izlogujte se</a></li>
            </ul>
            <?php
            } else {
            ?>
            <form class="navbar-form navbar-right" action="/login" method="post">
                <div class="form-group">
                    <input type="text" placeholder="Mail adresa" class="form-control" name="username">
                </div>
                <div class="form-group">
                    <input type="password" placeholder="Šifra" class="form-control" name="password">
                </div>
                <button type="submit" class="btn btn-success">Ulogujte se</button>
            </form>
            <?php
            }
            ?>
        </div>
    </div>
</nav>