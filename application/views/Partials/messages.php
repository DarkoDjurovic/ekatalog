<?php
if(isset($viewData['messages']) && is_array($viewData['messages'])) {
	?>
	<div class="messagesContainer">
	<?php
	foreach ($viewData['messages'] as $message) {
		?>
		<div class="alert alert-info messages">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<?php echo $message; ?>
		</div>
		<?php
	}
	?>
	</div>
	<?php
}
?>