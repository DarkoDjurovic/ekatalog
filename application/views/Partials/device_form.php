<form action="/device/<?php echo $viewData['mode']; ?>" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label for="name">Ime</label>
		<input type="text" class="form-control" name="name" id="name" maxlength="50" required value="<?php echo $viewData['device']->name; ?>"/>
	</div>
	<div class="form-group">
		<label for="description">Opis</label>
		<textarea class="form-control" name="description" id="description" required><?php echo $viewData['device']->description; ?></textarea>
	</div>
	<div class="form-group">
		<label for="price">Cena</label>
		<input type="number" class="form-control" name="price" id="price" min="1" max="1000000" step="1" required value="<?php echo $viewData['device']->price; ?>"/>
	</div>
	<div class="form-group">
		<label for="manufacturer">Proizvođač</label>
		<select class="form-control" name="manufacturer" id="manufacturer">
			<?php
			foreach($viewData['manufacturers'] as $manufacturer){
			?>
				<option value="<?php echo $manufacturer->id; ?>"
				        <?php
                        if(isset($viewData['device']->manufacturer) && $manufacturer->id === $viewData['device']->manufacturer->id)
                            echo 'selected';
                        ?>
				><?php echo $manufacturer->name; ?></option>
			<?php
			}
			?>
		</select>
	</div>
	<div class="form-group">
		<label for="offer_type">Posebna ponuda</label>
		<select class="form-control" name="offer_type" id="offer_type">
			<option value="">Nijedna</option>
			<?php
			foreach($viewData['offer_types'] as $offer_type){
			?>
				<option value="<?php echo $offer_type->id; ?>"
				        <?php
                        if(isset($viewData['device']->offer) && $offer_type->id === $viewData['device']->offer->id)
                            echo 'selected';
                        ?>
				><?php echo $offer_type->name; ?></option>
			<?php
			}
			?>
		</select>
	</div>
	<div class="form-group">
		<label for="categories">Kategorije</label>
		<select class="form-control" name="categories[]" id="categories" multiple>
			<?php
			$category_array = array();
			foreach($viewData['device']->categories as $cat){
				$category_array[] = $cat->id;
			}
			foreach($viewData['categories'] as $category){
			?>
				<option value="<?php echo $category->id; ?>"
				        <?php
                        if(in_array($category->id, $category_array))
                            echo 'selected';
                        ?>
				><?php echo $category->name; ?></option>
			<?php
			}
			?>
		</select>
	</div>
	<?php
	if(!isset($viewData['device']->id)) {
	?>
	<div class="form-group">
		<label for="image">Unesite glavnu sliku</label>
		<input type="file" name="image" id="image"/>
	</div>
	<div class="form-group">
		<label for="image">Unesite ostale slike</label>
		<input type="file" name="images[]" id="images" multiple/>
	</div>
	<input type="submit" class="btn btn-primary" name="insert" value="Sačuvaj" />
	<?php
	} else {
	?>
	<input type="submit" class="btn btn-primary" name="edit" value="Sačuvaj" />
	<?php
	}
	?>
</form>