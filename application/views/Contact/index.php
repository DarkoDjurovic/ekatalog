<?php
require(Helper::includePartial("open_document"));
require(Helper::includePartial("head"));
?>
<body>
<?php
require(Helper::includePartial("navigation"));
require(Helper::includePartial("messages"));
?>
	<main>
		<div class="jumbotron">
		    <div class="container">
		        <h1>Kontakt</h1>

		        <p>Pošaljite nam poruku ili nas kontaktirajte putem email adrese <a href="mailto:kontakt@ekatalog.com">kontakt@ekatalog.com</a>!</p>
		    </div>
		</div>
		<div class="container">
			<form>
				<label for="name">
					Ime i prezime
				</label>
				<input type="text" class="form-control" id="name" name="name" placeholder="Unesite ime i prezime" />

				<label for="email">
					Email adresa
				</label>
				<input type="email" class="form-control" id="email" name="email" placeholder="Unesite email adresu" />

				<label for="message">
					Poruka
				</label>
				<textarea id="message" class="form-control"></textarea>

				<button type="button" class="btn btn-success">Pošalji</button>
			</form>

			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2833.049481513365!2d20.494380915243873!3d44.75940557909895!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475a70e01f084941%3A0x31dff72cba2cff83!2z0KPQvdC40LLQtdGA0LfQuNGC0LXRgiDQodC40L3Qs9C40LTRg9C90YPQvCAtINCa0YPQvNC-0LTRgNCw0YjQutCwIDI2MQ!5e0!3m2!1ssr!2srs!4v1477744963236" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
	</main>
<?php
require(Helper::includePartial("include_scripts"));
?>
</body>
<?php
require(Helper::includePartial("close_document"));
?>