<?php
require(Helper::includePartial("open_document"));
require(Helper::includePartial("head"));
?>
<body>
<?php
require(Helper::includePartial("navigation"));
require(Helper::includePartial("messages"));
?>
	<main>
		<div class="jumbotron">
		    <div class="container">
		        <h1>E-katalog!</h1>

		        <p> E-katalog online prodavnica: najjeftiniji laptopovi, tableti na akciji, telefoni za 1 dinar.
					Dodatne pogodnosti: odloženo plaćanje, plaćanje na rate, plaćanje na čekove ili karticom. Garantovan kvalitet i brza besplatna isporuka.
					Pitate se gde kupiti tablet, laptop ili smart telefon? Odgovor je uvek E-katalog!
		        </p>
		    </div>
		</div>
		<div class="container">
		    <div class="row">
		        <div class="col-md-3">
			        <form action="/search/" method="get">
			            <div class="row">
			                <div class="col-md-12">
			                    <h2>Cena:</h2>
			                    <div class="row">
			                        <div class="col-md-6">
			                            <p>Od:</p>
			                        </div>
			                        <div class="col-md-6">
			                            <input type="number" size="3" style="width: 100px" name="price_from" value="<?php echo isset($viewData['price_from']) ? $viewData['price_from'] : ''; ?>"/>
			                        </div>
			                    </div>
			                    <div class="row">
			                        <div class="col-md-6">
			                            <p>Do:</p>
			                        </div>
			                        <div class="col-md-6">
			                            <input type="number" size="3" style="width: 100px" name="price_to" value="<?php echo isset($viewData['price_to']) ? $viewData['price_to'] : ''; ?>"/>
			                        </div>
			                    </div>
			                </div>

			                <div class="col-md-12">
			                    <h2>Vrsta ponude:</h2>
			                    <?php
			                    if(!isset($viewData['offer_types_selected']))
				                    $viewData['offer_types_selected'] = array();

				                foreach($viewData['offer_types'] as $offer_type){
					                ?>
						        <div class="row">
			                        <div class="col-md-6">
			                            <p><?php echo $offer_type->name; ?></p>
			                        </div>
			                        <div class="col-md-6">
			                            <input type="checkbox" value="<?php echo $offer_type->id; ?>" name="offer_type[]"
					                            <?php
					                            if(in_array($offer_type->id, $viewData['offer_types_selected']))
						                            echo 'checked';
					                            ?>
			                            />
			                        </div>
			                    </div>
					                <?php
				                }
				                ?>
			                </div>

			                <div class="col-md-12">
			                    <h2>Proizvođač:</h2>
				                <?php
				                if(!isset($viewData['manufacturers_selected']))
				                    $viewData['manufacturers_selected'] = array();

				                foreach($viewData['manufacturers'] as $manufacturer){
					                ?>
						        <div class="row">
			                        <div class="col-md-6">
			                            <p><?php echo $manufacturer->name; ?></p>
			                        </div>
			                        <div class="col-md-6">
				                        <input type="checkbox" value="<?php echo $manufacturer->id; ?>" name="manufacturer[]"
					                            <?php
					                            if(in_array($manufacturer->id, $viewData['manufacturers_selected']))
						                            echo 'checked';
					                            ?>
			                            />
			                        </div>
			                    </div>
					                <?php
				                }
				                ?>
			                </div>
			            </div>
				        <button class="btn btn-info">Pretraga</button>
			        </form>
		        </div>
		        <div class="col-md-9">
			        <?php
			        foreach($viewData['devices'] as $device){
				        require(Helper::includePartial("device_small"));
			        }
			        ?>
			    </div>
		    </div>
			<?php
			require(Helper::includePartial("footer"));
			?>
        </div>
	</main>
<?php
require(Helper::includePartial("include_scripts"));
?>
</body>
<?php
require(Helper::includePartial("close_document"));
?>