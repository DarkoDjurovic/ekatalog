<?php
require(Helper::includePartial("open_document"));
require(Helper::includePartial("head"));
?>
<body>
<?php
require(Helper::includePartial("navigation"));
require(Helper::includePartial("messages"));
?>
	<main>
		<div class="jumbotron">
		    <div class="container">
		        <h1><?php echo $viewData['device']->name; ?></h1>

		        <p><?php echo $viewData['device']->description; ?></p>
		    </div>
		</div>
		<div class="container">
			<div class="col-md-12">
		        <?php
		        $device = $viewData['device'];
		        require(Helper::includePartial("device_large"));
		        ?>
		    </div>
        </div>
	</main>
<?php
require(Helper::includePartial("include_scripts"));
require(Helper::includePartial("gallery_menu"));
?>
</body>
<?php
require(Helper::includePartial("close_document"));
?>