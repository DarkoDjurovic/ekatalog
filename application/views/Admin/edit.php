<?php
require(Helper::includePartial("open_document"));
require(Helper::includePartial("head"));
?>
<body>
<?php
require(Helper::includePartial("navigation"));
require(Helper::includePartial("messages"));
?>
	<main>
		<div class="jumbotron">
		    <div class="container">
		        <h1>Unos proizvoda</h1>
		    </div>
		</div>
		<div class="container">
		    <div class="row">
		        <div class="col-md-12">
			        <?php
			        require(Helper::includePartial("device_form"));
			        ?>
			    </div>
		    </div>
			<?php
			require(Helper::includePartial("footer"));
			?>
        </div>
	</main>
<?php
require(Helper::includePartial("include_scripts"));
?>
</body>
<?php
require(Helper::includePartial("close_document"));
?>