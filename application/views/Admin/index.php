<?php
require(Helper::includePartial("open_document"));
require(Helper::includePartial("head"));
?>
<body>
<?php
require(Helper::includePartial("navigation"));
require(Helper::includePartial("messages"));
?>
	<main>
		<div class="jumbotron">
		    <div class="container">
		        <h1>Admin panel</h1>
			    <form class="navbar-form" action="/admin" method="get" style="display: inline-block">
				    <input type="text" class="form-control" name="search" value="<?php echo !empty($viewData['search']) ? $viewData['search'] : ""; ?>" />
				    <button class="btn btn-info">Pretraga</button>
			    </form>
			    <form class="navbar-form" action="/device/new" method="post" style="display: inline-block">
				    <button class="btn btn-success">Nov uređaj</button>
			    </form>
		    </div>
		</div>
		<div class="container">
		    <div class="row">
		        <div class="col-md-12">
			        <?php
			        foreach($viewData['devices'] as $device){
				        require(Helper::includePartial("device_small_admin"));
			        }
			        ?>
			    </div>
		    </div>
			<?php
			require(Helper::includePartial("footer"));
			?>
        </div>
	</main>
<?php
require(Helper::includePartial("include_scripts"));
?>
</body>
<?php
require(Helper::includePartial("close_document"));
?>