<?php

/**
 * model administratora
 */
class AdminModel{
    /**
     * @var string
     */
	private static $table = 'admins';

    /**
     * dohvatanje svih administratora
     * @return array
     */
	public static function getAll() {
        $SQL = 'SELECT * FROM '.self::$table.' ORDER BY username;';
        $prep = DB::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * dohvatanje jednog administratora
     * @param $id
     * @return mixed
     */
	public static function getById($id) {
        $SQL = 'SELECT * FROM '.self::$table.' WHERE id = ?;';
        $prep = DB::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    /**
     * dohvatanje jednog administratora
     * @param $username
     * @param $password
     * @return mixed
     */
	public static function getByUsernameAndPassword($username, $password) {
		$passHash = hash('sha512', $password . Config::SALT);
		$SQL = 'SELECT * FROM '.self::$table.' WHERE username = ? AND password = ?;';
        $prep = DB::getInstance()->prepare($SQL);
        $prep->execute([$username, $passHash]);
        return $prep->fetch(PDO::FETCH_OBJ);
	}

    /**
     * provera da li je administrator aktivan
     * @param $id
     * @return mixed
     */
	public static function isActive($id){
		$SQL = 'SELECT * FROM '.self::$table.' WHERE id = ? AND active = 1;';
        $prep = DB::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetch(PDO::FETCH_OBJ);
	}
}