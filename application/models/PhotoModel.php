<?php

/**
 * model slika
 */
class PhotoModel {
	/**
	 * @var string
	 */
    private static $table = 'photos';

	/**
	 * dohvatanje svih fotografija
	 * @return array
	 */
    public static function getAll() {
        $SQL = 'SELECT file_name FROM '.self::$table.' ORDER BY file_name;';
        $prep = DB::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

	/**
	 * dohvatanje jedne fotografije
	 * @param $id
	 * @return mixed
	 */
    public static function getById($id) {
        $id = intval($id);
        $SQL = 'SELECT file_name FROM '.self::$table.' WHERE id = ?;';
        $prep = DB::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

	/**
	 * dohvatanje svih fotografija za uredjaj
	 * @param $id
	 * @return array
	 */
    public static function getByDeviceId($id) {
        $id = intval($id);
        $SQL = 'SELECT '.self::$table.'.id, '.self::$table.'.file_name FROM '.self::$table.'
         JOIN device_photo ON device_photo.photo_id = '.self::$table.'.id
         JOIN devices ON devices.id = device_photo.device_id
         WHERE devices.id = ?;';
        $prep = DB::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

	/**
	 * preuredjivanje ulaznih $_FILE nizova tako da se lakse obradjuju
	 * @param $file_post
	 * @return array
	 */
    public static function reArrayFiles(&$file_post) {

        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }

	/**
	 * upload jedne fotografije
	 * @param $field
	 * @return null|string
	 */
    public static function upload(&$field){
        if (!$_FILES or !isset($field)) return null;

        if ($field['error'] != 0) {
            //Session::addMessage('Greska prilikom dodavanja fajla!');
            return null;
        }

        $temporaryPath = $field['tmp_name'];
        $fileSize      = $field['size'];
        $originalName  = $field['name'];

        if ($fileSize > 1024*1024) {
            Session::addMessage('Fajl koji dodajete je veci od 1MB!');
            return null;
        }

        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $mimeType = $finfo->file($temporaryPath);

        if ($mimeType != 'image/jpeg') {
            Session::addMessage('Dozvojeno je dodavanje samo JPG slika!');
            return null;
        }

        $baseName = strtolower($originalName);
        $baseName = preg_replace('[^a-z0-9\- ]', '', $baseName);
        $baseName = preg_replace(' +', '-', $baseName);

        $fileName = $baseName . '_' . str_replace(' ', '', microtime()) . '.jpg';

        $newLocation = Config::PHOTO_LIBRARY_PATH . $fileName;

        $res = move_uploaded_file($temporaryPath, $newLocation);
        if (!$res) {
            Session::addMessage('Greska prilikom snimanja fajla!');
            return null;
        }

        if(!self::resize_image($newLocation, null, 200, 200, true, 'file', $newLocation.'.thumb.jpg', false, false, 100, false)){
	        Session::addMessage('Greska prilikom smanjivanja fajla!');
            return null;
        }

        return $fileName;
    }

	/**
	 * dodavanje fotografije
	 * @param $file_name
	 * @param $device_id
	 * @param $is_main
	 * @return bool
	 */
    public static function add($file_name, $device_id, $is_main) {
	    $ret = true;
        $SQL = 'INSERT INTO '.self::$table.' (id, file_name, is_main) VALUES (null, ?, ?);';
        $prep = DB::getInstance()->prepare($SQL);
        $ret = $ret && $prep->execute([$file_name, $is_main]);

        $photo_id = DB::getInstance()->lastInsertId();

        $SQL = 'INSERT INTO device_photo (device_id, photo_id) VALUES (?, ?);';
        $prep = DB::getInstance()->prepare($SQL);
        $ret = $ret && $prep->execute([$device_id, $photo_id]);
	    return $ret;
    }

	/**
	 * brisanje svih fotografija za uredjaj
	 * @param $id
	 * @return bool
	 */
	public static function deleteById($id){
		$ret = true;
		$SQL = 'DELETE FROM '.self::$table.' WHERE id = ?;';
        $prep = DB::getInstance()->prepare($SQL);
        $ret = $ret && $prep->execute([$id]);

		$SQL = 'DELETE FROM device_photos WHERE photo_id = ?;';
        $prep = DB::getInstance()->prepare($SQL);
        $ret = $ret && $prep->execute([$id]);
		return $ret;
	}

	public static function deleteByDeviceId($id){
		$ret = true;
		$photosForDevice = self::getByDeviceId($id);

		foreach($photosForDevice as $photo){
			$ret = $ret && self::deleteById($photo->id);
		}
		return $ret;
	}

	/**
	 * funkcija za smanjivanje slike
	 * @param  $file - file name to resize
	 * @param  $string - The image data, as a string
	 * @param  $width - new image width
	 * @param  $height - new image height
	 * @param  $proportional - keep image proportional, default is no
	 * @param  $output - name of the new file (include path if needed)
	 * @param  $delete_original - if true the original image will be deleted
	 * @param  $use_linux_commands - if set to true will use "rm" to delete the image, if false will use PHP unlink
	 * @param  $quality - enter 1-100 (100 is best quality) default is 100
	 * @param  $grayscale - if true, image will be grayscale (default is false)
	 * @return boolean|resource
	 */
	function resize_image($file,
	                            $string = null,
	                            $width = 0,
	                            $height = 0,
	                            $proportional = false,
	                            $output = 'file',
								$outputDestination = null,
	                            $delete_original = true,
	                            $use_linux_commands = false,
	                            $quality = 100,
	                            $grayscale = false
	)
	{

		if ($height <= 0 && $width <= 0) return false;
		if ($file === null && $string === null) return false;
		# Setting defaults and meta
		$info = $file !== null ? getimagesize($file) : getimagesizefromstring($string);
		$image = '';
		$final_width = 0;
		$final_height = 0;
		list($width_old, $height_old) = $info;
		$cropHeight = $cropWidth = 0;
		# Calculating proportionality
		if ($proportional) {
			if ($width == 0) $factor = $height / $height_old;
			elseif ($height == 0) $factor = $width / $width_old;
			else                    $factor = min($width / $width_old, $height / $height_old);
			$final_width = round($width_old * $factor);
			$final_height = round($height_old * $factor);
		} else {
			$final_width = ($width <= 0) ? $width_old : $width;
			$final_height = ($height <= 0) ? $height_old : $height;
			$widthX = $width_old / $width;
			$heightX = $height_old / $height;

			$x = min($widthX, $heightX);
			$cropWidth = ($width_old - $width * $x) / 2;
			$cropHeight = ($height_old - $height * $x) / 2;
		}
		# Loading image to memory according to type
		switch ($info[2]) {
			case IMAGETYPE_JPEG:
				$file !== null ? $image = imagecreatefromjpeg($file) : $image = imagecreatefromstring($string);
				break;
			case IMAGETYPE_GIF:
				$file !== null ? $image = imagecreatefromgif($file) : $image = imagecreatefromstring($string);
				break;
			case IMAGETYPE_PNG:
				$file !== null ? $image = imagecreatefrompng($file) : $image = imagecreatefromstring($string);
				break;
			default:
				return false;
		}

		# Making the image grayscale, if needed
		if ($grayscale) {
			imagefilter($image, IMG_FILTER_GRAYSCALE);
		}

		# This is the resizing/resampling/transparency-preserving magic
		$image_resized = imagecreatetruecolor($final_width, $final_height);
		if (($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG)) {
			$transparency = imagecolortransparent($image);
			$palletsize = imagecolorstotal($image);
			if ($transparency >= 0 && $transparency < $palletsize) {
				$transparent_color = imagecolorsforindex($image, $transparency);
				$transparency = imagecolorallocate($image_resized, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
				imagefill($image_resized, 0, 0, $transparency);
				imagecolortransparent($image_resized, $transparency);
			} elseif ($info[2] == IMAGETYPE_PNG) {
				imagealphablending($image_resized, false);
				$color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
				imagefill($image_resized, 0, 0, $color);
				imagesavealpha($image_resized, true);
			}
		}
		imagecopyresampled($image_resized, $image, 0, 0, $cropWidth, $cropHeight, $final_width, $final_height, $width_old - 2 * $cropWidth, $height_old - 2 * $cropHeight);


		# Taking care of original, if needed
		if ($delete_original) {
			if ($use_linux_commands) exec('rm ' . $file);
			else @unlink($file);
		}
		# Preparing a method of providing result
		switch (strtolower($output)) {
			case 'browser':
				$mime = image_type_to_mime_type($info[2]);
				header("Content-type: $mime");
				$output = NULL;
				break;
			case 'file':
				$output = $outputDestination;
				break;
			case 'return':
				return $image_resized;
				break;
			default:
				break;
		}

		# Writing image according to type to the output destination and image quality
		switch ($info[2]) {
			case IMAGETYPE_GIF:
				imagegif($image_resized, $output);
				break;
			case IMAGETYPE_JPEG:
				imagejpeg($image_resized, $output, $quality);
				break;
			case IMAGETYPE_PNG:
				$quality = 9 - (int)((0.9 * $quality) / 10.0);
				imagepng($image_resized, $output, $quality);
				break;
			default:
				return false;
		}
		return true;
	}
}
