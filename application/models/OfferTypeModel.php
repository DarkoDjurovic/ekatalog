<?php

/**
 * model tipa ponude
 */
class OfferTypeModel{
	/**
	 * @var string
	 */
	private static $table = 'offer_type';

	/**
	 * dohvatanje svih tipova ponude
	 * @return array
	 */
	public static function getAll() {
		$SQL = 'SELECT * FROM '.self::$table.' ORDER BY name;';
        $prep = DB::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
	}

	/**
	 * dohvatnaje jednog tipa ponude
	 * @param $id
	 * @return mixed
	 */
	public static function getById($id) {
		$id = intval($id);
        $SQL = 'SELECT * FROM '.self::$table.' WHERE id = ?;';
        $prep = DB::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetch(PDO::FETCH_OBJ);
	}

	/**
	 * dodavanje tipa ponude
	 * @param $name
	 * @return bool
	 */
	public static function add($name){
		$SQL = 'INSERT INTO '.self::$table.' (id, name)
		VALUES (null, ?);';
        $prep = DB::getInstance()->prepare($SQL);
        return $prep->execute([$name]);
	}

	/**
	 * izmena tipa ponude
	 * @param $id
	 * @param $name
	 * @return bool
	 */
	public static function edit($id, $name){
		$SQL = 'UPDATE '.self::$table.' SET name = ? WHERE id = ?;';
        $prep = DB::getInstance()->prepare($SQL);
        return $prep->execute([$name, $id]);
	}

	/**
	 * brisanje tipa ponude
	 * @param $id
	 * @return bool
	 */
	public static function delete($id){
		$SQL = 'DELETE FROM '.self::$table.' WHERE id = ?;';
        $prep = DB::getInstance()->prepare($SQL);
        return $prep->execute([$id]);
	}
}