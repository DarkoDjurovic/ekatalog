<?php

/**
 * model kategorija
 */
class CategoryModel{
	/**
	 * @var string
	 */
	private static $table = 'categories';

	/**
	 * dohvatanje svih kategorija
	 * @return array
	 */
	public static function getAll() {
		$SQL = 'SELECT * FROM '.self::$table.' ORDER BY name;';
        $prep = DB::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
	}

	/**
	 * dohvatanje jedne kategorije
	 * @param $id
	 * @return mixed
	 */
	public static function getById($id) {
		$id = intval($id);
        $SQL = 'SELECT * FROM '.self::$table.' WHERE id = ?;';
        $prep = DB::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetch(PDO::FETCH_OBJ);
	}

	/**
	 * dohvatanje svih kategorija za uredjaj
	 * @param $id
	 * @return array
	 */
	public static function getByDeviceId($id) {
		$id = intval($id);
        $SQL = 'SELECT * FROM '.self::$table.' a LEFT JOIN device_category b ON a.id = b.category_id WHERE b.device_id = ?;';
        $prep = DB::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
	}

	/**
	 * dodavanje kategorije
	 * @param $name
	 * @return bool
	 */
	public static function add($name){
		$SQL = 'INSERT INTO '.self::$table.' (id, name)
		VALUES (null, ?);';
        $prep = DB::getInstance()->prepare($SQL);
        return $prep->execute([$name]);
	}

	/**
	 * izmena kategorije
	 * @param $id
	 * @param $name
	 * @return bool
	 */
	public static function edit($id, $name){
		$SQL = 'UPDATE '.self::$table.' SET name = ? WHERE id = ?;';
        $prep = DB::getInstance()->prepare($SQL);
        return $prep->execute([$name, $id]);
	}

	/**
	 * brisanje kategorije
	 * @param $id
	 * @return bool
	 */
	public static function delete($id){
		$SQL = 'DELETE FROM '.self::$table.' WHERE id = ?;';
        $prep = DB::getInstance()->prepare($SQL);
        return $prep->execute([$id]);
	}
}