<?php

/**
 * Model uredjaja (artikla)
 */
class DeviceModel
{
	/**
	 * @var string
	 */
	private static $table = 'devices';

	/**
	 * funkcija koja proverava validnost unetih podataka u formi
	 * @param $name
	 * @param $description
	 * @param $manufacturer
	 * @param $offer_type_id
	 * @param $price
	 * @param $categories
	 * @return bool
	 */
	private static function validate(&$name, &$description, &$manufacturer, &$offer_type_id, &$price, &$categories)
	{
		if(isset($name))
			$name = filter_var($name, FILTER_SANITIZE_STRING);
		if(isset($description))
			$description = filter_var($description, FILTER_SANITIZE_STRING);
		if(isset($price))
			$price = filter_var($price, FILTER_VALIDATE_INT);
		if(isset($manufacturer))
			$manufacturer = filter_var($manufacturer, FILTER_VALIDATE_INT);
		if(isset($offer_type_id))
			$offer_type_id = filter_var($offer_type_id, FILTER_VALIDATE_INT);
		if(isset($categories))
			$categories = filter_var_array($categories, FILTER_VALIDATE_INT);

		return (isset($name) && $name !== false)
		&& (isset($description) && $description !== false)
		&& (isset($price) && $price !== false)
		&& (isset($manufacturer) && $manufacturer !== false)
		&& (!isset($offer_type_id) || (isset($offer_type_id) && $offer_type_id !== false))
		&& (!isset($categories) || (isset($categories) && $categories !== false));
	}

	/**
	 * funkcija koja priprema select-ovane objekte dopunjujuci im strukturu
	 * @param $device
	 */
	private static function populateFields($device)
	{
		$devicePhotos = PhotoModel::getByDeviceId($device->id);
		if (isset($devicePhotos[0])) {
			$device->photo = Helper::getImage($devicePhotos[0]);
			$device->photo_thumb = Helper::getImageThumb($devicePhotos[0]);
		}
		else {
			$device->photo = Helper::getImage(null);
			$device->photo_thumb = Helper::getImageThumb(null);
		}

		$device->photos = array();
		foreach ($devicePhotos as $dP) {
			$device->photos[] = Helper::getImageThumb($dP);
		}
		$device->photos_thumb = array();
		foreach ($devicePhotos as $dP) {
			$device->photos_thumb[] = Helper::getImageThumb($dP);
		}

		if ($device->offer_type_id) {
			$device->offer = OfferTypeModel::getById($device->offer_type_id);
		} else {
			$device->offer = null;
		}

		$device->categories = CategoryModel::getByDeviceId($device->id);


		$device->manufacturer = ManufacturerModel::getById($device->manufacturer);
	}

	/**
	 * dohvatanje svih uredjaja
	 * @return array
	 */
	public static function getAll()
	{
		$SQL = 'SELECT * FROM ' . self::$table . ' ORDER BY id;';
		$prep = DB::getInstance()->prepare($SQL);
		$prep->execute();
		$allDevices = $prep->fetchAll(PDO::FETCH_OBJ);
		foreach ($allDevices as $device)
			self::populateFields($device);
		return $allDevices;
	}

	/**
	 * dohvatanje jednog uredjaja
	 * @param $id
	 * @return mixed
	 */
	public static function getById($id)
	{
		$id = intval($id);
		$SQL = 'SELECT * FROM ' . self::$table . ' WHERE id = ?;';
		$prep = DB::getInstance()->prepare($SQL);
		$prep->execute([$id]);
		$device = $prep->fetch(PDO::FETCH_OBJ);
		self::populateFields($device);
		return $device;
	}

	/**
	 * dohvatanje jedne stranice (sa uslovom)
	 * @param $where
	 * @param $catId
	 * @param $page
	 * @param int $per_page
	 * @return array
	 */
	public static function getPagedWithCondition($where, $catId, $page, $per_page = Config::DEFAULT_PER_PAGE)
	{
		if ($page <= 0)
			$page = 1;
		$SQL = 'SELECT * FROM ' . self::$table . ' a';
		if ($catId > 0) {
			$SQL .= ' LEFT JOIN device_category b ON a.id = b.device_id WHERE b.category_id = ' . $catId;
		}
		if (!empty($where)) {
			if ($catId <= 0)
				$SQL .= " WHERE ($where)";
			else
				$SQL .= " AND ($where)";
		}
		$SQL .= ' ORDER BY id LIMIT ' . (($page - 1) * $per_page) . ', ' . $per_page . ';';
		$prep = DB::getInstance()->prepare($SQL);
		$prep->execute();
		$pagedDevices = $prep->fetchAll(PDO::FETCH_OBJ);
		foreach ($pagedDevices as $device)
			self::populateFields($device);
		return $pagedDevices;
	}

	/**
	 * dohvatanje jedne stranice
	 * @param $catId
	 * @param $page
	 * @param int $per_page
	 * @return array
	 */
	public static function getPaged($catId, $page, $per_page = Config::DEFAULT_PER_PAGE)
	{
		return self::getPagedWithCondition("", $catId, $page, $per_page);
	}

	/**
	 * dodavanje uredjaja
	 * @param $name
	 * @param $description
	 * @param $manufacturer
	 * @param $offer_type_id
	 * @param $price
	 * @param $categories
	 * @return bool|string
	 */
	public static function add($name, $description, $manufacturer, $offer_type_id, $price, $categories)
	{
		if (!self::validate($name, $description, $manufacturer, $offer_type_id, $price, $categories)) {
			Session::addMessage("Nevalidni podaci u formi!");
			return false;
		}
		$ret = true;
		$user_id = Auth::getUserId();
		$SQL = 'INSERT INTO ' . self::$table . ' (id, name, description, manufacturer, offer_type_id, price, user_id)
		VALUES (null, ?, ?, ?, ?, ?, ?);';
		$prep = DB::getInstance()->prepare($SQL);
		$ret = $ret && $prep->execute([$name, $description, $manufacturer, $offer_type_id, $price, $user_id]);
		$device_id = DB::getInstance()->lastInsertId();

		foreach ($categories as $category) {
			$SQL = 'INSERT INTO device_category (device_id, category_id) VALUES (?, ?);';
			$prep = DB::getInstance()->prepare($SQL);
			$ret = $ret && $prep->execute([$device_id, $category]);
		}

		if ($ret)
			return $device_id;
		else
			return false;
	}

	/**
	 * izmena uredjaja
	 * @param $id
	 * @param $name
	 * @param $description
	 * @param $manufacturer
	 * @param $offer_type_id
	 * @param $price
	 * @param $categories
	 * @return bool
	 */
	public static function edit($id, $name, $description, $manufacturer, $offer_type_id, $price, $categories)
	{
		if (!self::validate($name, $description, $manufacturer, $offer_type_id, $price, $categories)) {
			Session::addMessage("Nevalidni podaci u formi!");
			return false;
		}
		$ret = true;
		$user_id = Auth::getUserId();
		$SQL = 'UPDATE ' . self::$table . ' SET name = ?, description = ?, manufacturer = ?, offer_type_id = ?, price = ?, user_id = ?
		WHERE id = ?;';
		$prep = DB::getInstance()->prepare($SQL);
		$ret = $ret && $prep->execute([$name, $description, $manufacturer, $offer_type_id, $price, $user_id, $id]);

		$SQL = 'DELETE FROM device_category WHERE device_id = ?;';
		$prep = DB::getInstance()->prepare($SQL);
		$ret = $ret && $prep->execute([$id]);

		foreach ($categories as $category) {
			$SQL = 'INSERT INTO device_category (device_id, category_id) VALUES (?, ?);';
			$prep = DB::getInstance()->prepare($SQL);
			$ret = $ret && $prep->execute([$id, $category]);
		}

		return $ret;
	}

	/**
	 * brisanje uredjaja
	 * @param $id
	 * @return bool
	 */
	public static function delete($id)
	{
		$ret = true;
		$SQL = 'DELETE FROM ' . self::$table . ' WHERE id = ?';
		$prep = DB::getInstance()->prepare($SQL);
		$ret = $ret && $prep->execute([$id]);

		$ret = $ret && PhotoModel::deleteByDeviceId($id);
		return $ret;
	}
}